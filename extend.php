<?php

/*
 * This file is part of Flarum.
 *
 * For detailed copyright and license information, please view the
 * LICENSE file that was distributed with this source code.
 */

use Flarum\Extend;

return [
    // Register extenders here to customize your forum!
    (new Extend\Validator(\Flarum\User\UserValidator::class))
        ->configure(function ($flarumValidator, $validator) {
            $rules = $validator->getRules();

            // The validator is sometimes used to validate *only* other attributes, in which case the username rule won't be present. We can skip this situation by returning early
            if (!array_key_exists('username', $rules)) {
                return;
            }

            // Loop through all Laravel validation rules until we find the one we're looking to replace
            $rules['username'] = array_map(function (string $rule) {
                // Example regex change: allow dots in usernames
                if (\Illuminate\Support\Str::startsWith($rule, 'regex:')) {
                    return 'regex:/^[.a-z0-9_@-]+$/i';
                }

                // Example min length change
                if (\Illuminate\Support\Str::startsWith($rule, 'min:')) {
                    return 'min:6';
                }

                // If it's not one of the rules we're looking for, we keep it as-it
                return $rule;
            }, $rules['username']);

            $validator->setRules($rules);
        }),
];
